import { combineReducers } from "redux";

import mapReducer from "containers/App/reducers"

const appReducer = (injectedreducers) => {
  return combineReducers({
    mapReducer,
    ...injectedreducers,
  });
};

export default appReducer;
