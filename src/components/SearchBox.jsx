import {
  Close,
  KeyboardDoubleArrowDown,
  KeyboardDoubleArrowUp,
  History
} from "@mui/icons-material";
import { useEffect, useRef, useState } from "react";
import { Box, Grow } from "@mui/material";
import cx from "classnames";
import OptionControl from "./OptionControl";
import { useSelector } from "react-redux";
import { historyList } from "containers/App/reducers";

const SearchBox = (props) => {
  const data = useSelector(historyList)
  const [location, setLocation] = useState("")
  const input = useRef(null)
  const close = () => {
    setLocation("")
    setShow(false)
    props.close()
  }
  const [show, setShow] = useState(true)

  const handleControl = (value) => {
    props.handleControl(value)
  }

  const setMarker = (v) => {
    setLocation("")
    props.setMarker(v)
  }
  
  useEffect(() => {
      if (input.current) {
        input.current.focus()
      }

      if(window.localStorage.getItem('searchLocation')) {
        setLocation("")
        window.localStorage.removeItem("searchLocation")
      }
  })

  return (
    <div className="tw-w-max tw-max-w-[90vw] tw-border tw-border-solid tw-border-blue-500 tw-rounded-md tw-px-4 tw-pt-2 tw-bg-blue-500 tw-relative">
      <p className="tw-text-white tw-text-sm tw-font-light tw-italic">Going somewhere...?</p>
      <div>
        <Box>
          <Grow in={show} {...{timeout: show ? 1000:0}}>
            <div className={cx("tw-bg-white tw-rounded tw-p-0", !show ? 'tw-h-0':'')}>
              <div className="tw-text-center">Options</div>

              <div className="tw-border tw-p-2">
                <OptionControl handleControl={handleControl}/>
              </div>
            </div>
          </Grow>
        </Box>
      </div>
      
      <div className="tw-relative tw-my-2">
        <input
          type="text"
          placeholder="Search"
          id="pac-input"
          className="tw-rounded focus:tw-outline-none focus:tw-ring focus:tw-ring-orange-600 tw-px-4 tw-py-2 tw-max-w-full tw-w-96 tw-text-sm"
          ref={input}
          value={location}
          onChange={(e) => setLocation(e.target.value)}
        />
        <div className="tw-absolute tw-right-2 tw-top-2 tw-cursor-pointer tw-bg-gray-50 hover:tw-bg-gray-300 rounded" onClick={() => setLocation("")}>
          <Close />
        </div>
      </div>






      <div className="tw-w-full tw-flex tw-items-center tw-justify-between tw-gap-1 tw-text-white tw-text-sm py-2">
        {!show ? 
        (<span className="tw-cursor-pointer" onClick={() => setShow(true) }>
          <KeyboardDoubleArrowDown /> show options
        </span>)
        :
        (<span className="tw-cursor-pointer" onClick={() => setShow(false) }>
          <KeyboardDoubleArrowUp /> side sptions
        </span>)        
        }
        
        <div className="tw-cursor-pointer"><History />search history</div>
        <div className="tw-text-black hover:tw-font-semibold tw-cursor-pointer" onClick={close}>close</div>
      </div>
      
      {data.length > 0 &&
        <div className="tw-bg-blue-500 tw-pb-4">
          <div className="tw-bg-white tw-rounded tw-p-2 tw-max-w-[24rem] tw-w-full">
            <ol className="tw-list-decimal tw-list-inside tw-text-xs tw-space-y-2">
              {data.map((v, i) => <li key={i} className="tw-cursor-pointer hover:tw-text-blue-500" onClick={() => setMarker(v)}>{v.formatted_address}</li>)}
              
            </ol>
          </div>

        </div>
      }

    </div>
  );
};

export default SearchBox;
