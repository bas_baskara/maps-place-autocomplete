import { useEffect, useRef, useState } from "react";
import { useDispatch } from "react-redux";
import { GOOGLE_API_KEY } from "../utils";
import AlertNotification from "./Alert";
import InfoWindowBox from "./InfoWindow";
import { updateHistory } from "containers/App/reducers";


const Map = (props) => {
  const {types, currentPosition} = props
  const map = useRef(null);
  const autocomplete = useRef(null);
  const infoWindow = useRef(null);
  const marker = useRef(null);
  const infowindowContent = useRef(null)
  const [open, setOpen] = useState(false)
  const [placeName, setPlaceName] = useState("")
  const dispatch = useDispatch()

  useEffect(() => {
    if (autocomplete.current) {

      autocomplete.current.strictBounds = false
      autocomplete.current.unbind("bounds")

      if (types === "strict-bounds") {
        autocomplete.current.strictBounds = true
        autocomplete.current.bindTo("bounds", map.current)
      } else if (types === "bias") {
        autocomplete.current.bindTo("bounds", map.current)
      } else {
        autocomplete.current.types = types ? [types]:[]

      }
    }
  }, [types])

  useEffect(() => {
    if (currentPosition) {
      map.current.fitBounds(currentPosition.geometry.viewport);
      marker.current.setPosition(currentPosition.geometry.location);
      marker.current.setVisible(true);
      infowindowContent.current.children["place-name"].textContent = currentPosition.name;
      infowindowContent.current.children["place-address"].textContent =
      currentPosition.formatted_address;
        infoWindow.current.open(map.current, marker.current);
    }
  }, [currentPosition])

  useEffect(() => {
    if (open) {
      setTimeout(() => {
        setOpen(false)
        setPlaceName("")
      }, 3000);
    }
  }, [open])

  useEffect(() => {
    if (!window.initMap) {
      loadScript(
        `https://maps.googleapis.com/maps/api/js?key=${GOOGLE_API_KEY}&libraries=places&callback=initMap`
      );
      window.initMap = initMap;
    }
  }, []);

  const loadScript = (url) => {
    const script = window.document.createElement("script");
    script.src = url;
    script.async = true;
    script.defer = true;
    window.document.head.appendChild(script);
  };

  const initMap = () => {
    map.current = new window.google.maps.Map(document.getElementById("map"), {
      center: { lat: -7.797068, lng: 110.370529 },
      zoom: 13,
      mapTypeControl: false,
    });

    const options = {
      fields: ["formatted_address", "geometry", "name"],
      strictBounds: false,
      types: [],
    };

    const input = document.getElementById("pac-input")
    if (input) {
      autocomplete.current = new window.google.maps.places.Autocomplete(input, options);
      autocomplete.current.bindTo("bounds", map.current);
      autocomplete.current.setTypes(types)
    }
    infowindowContent.current = document.getElementById("infowindow-content");
    if (infowindowContent.current) {
      infoWindow.current = new window.google.maps.InfoWindow();
      infoWindow.current.setContent(infowindowContent.current);

    }

    if (map.current) {
      marker.current = new window.google.maps.Marker({
        map: map.current,
        anchorPoint: new window.google.maps.Point(0, -29),
      });
    }

    if (autocomplete.current) {
      autocomplete.current.addListener("place_changed", () => {
        infoWindow.current.close();
        marker.current.setVisible(false);

        const place = autocomplete.current.getPlace();

        if (!place.geometry || !place.geometry.location) {
          setOpen(true)
          setPlaceName(place.name)
          return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
          map.current.fitBounds(place.geometry.viewport);
        } else {
          map.current.setCenter(place.geometry.location);
          map.current.setZoom(17);
        }

        marker.current.setPosition(place.geometry.location);
        marker.current.setVisible(true);
        infowindowContent.current.children["place-name"].textContent = place.name;
        infowindowContent.current.children["place-address"].textContent =
          place.formatted_address;
          infoWindow.current.open(map.current, marker.current);
        
        dispatch(updateHistory(JSON.stringify(place)))
        window.localStorage.setItem('searchLocation', 'clear')
      })
    }
  };

  return (
    <>
      <div
        id="map"
        className="tw-w-full tw-h-full"
      ></div>
      <InfoWindowBox />
      <AlertNotification open={open} place={placeName} />

    </>
  );
};

export default Map;
