import { Snackbar, Slide } from "@mui/material";


function SlideTransition(props) {
  return <Slide {...props} direction="down" />;
}

const AlertNotification = (props) => {
	const {open, place} = props

	return (
		<div>
			<Snackbar
				anchorOrigin={{ vertical: 'top', horizontal:'center' }}
				open={open}
				TransitionComponent={SlideTransition}
				message={`No details available for input: '${place}'`}
				key={"top-center"}
			/>

		</div>
	)
}

export default AlertNotification