import { Drawer, Button } from "@mui/material";
import { Search } from "@mui/icons-material";
import { useState } from "react";
import { ChevronDoubleLeft } from "assets/icons";
import { ReactSVG } from "react-svg";

const Sidebar = () => {
  const [open, setOpen] = useState(true);

  return (
    <div>
      <Button onClick={() => setOpen(!open)}>
        Search <Search />
      </Button>

      <Drawer anchor={"left"} open={open} className>
        <div className="tw-bg-white tw-min-w-[20rem] tw-min-h-screen">
          <div className="tw-bg-blue-500 tw-text-white tw-relative tw-text-2xl tw-font-semibold tw-py-2 tw-text-center">
            <span>Filter</span>
            <div className="tw-absolute tw-top-2 tw-right-2">
              <Button onClick={() => setOpen(false)}>
                <ReactSVG src={ChevronDoubleLeft} />
              </Button>
            </div>
          </div>
        </div>
      </Drawer>
    </div>
  );
};

export default Sidebar;
