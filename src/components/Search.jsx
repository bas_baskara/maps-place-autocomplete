import {
  Search as SearchIcon
} from "@mui/icons-material";


import Zoom from '@mui/material/Zoom';
import Box from '@mui/material/Box';

import { useState } from "react";
import cx from 'classnames'

import SearchBox from './SearchBox';



const Search = (props) => {
  const [show, setShow] = useState(false)

  const handleControl = (value) => {
    props.handleControl(value)
  }

  const setMarker = (v) => {
    props.setMarker(v)
  }
  return (
    <div className="tw-grid tw-grid-cols-1">
      <Box className={cx("tw-col-start-1 tw-col-end-2 tw-row-start-1 tw-row-end-1", !show ? 'tw-z-10':'')}>
        <Zoom in={!show}>
          <div className={cx("tw-bg-white/75 hover:tw-bg-blue-500/75 tw-text-gray-700 hover:tw-text-white tw-p-2 tw-w-max tw-rounded-lg tw-text-black tw-cursor-pointer tw-grid tw-place-content-center transition duration-700")}>
            <div onClick={() => setShow(true)}>
            <span className="tw-text-sm tw-font-medium">Search</span>
            <SearchIcon fontSize="medium" />
            </div>
          </div>
        </Zoom>
      </Box>

      <div className={cx("tw-col-start-1 tw-col-end-2 tw-row-start-1 tw-row-end-1", show ? 'tw-z-10':'')}>
        <Box sx={{ width: 100, height: 100 }}>
          <Zoom in={show}>
          <div><SearchBox close={() => setShow(false)} handleControl={handleControl} setMarker={setMarker} /></div>
          </Zoom>

        </Box>

      </div>
    
    </div>
  );
};

export default Search;
