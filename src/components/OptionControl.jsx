import {Radio, RadioGroup, FormControlLabel, FormControl} from '@mui/material';
import { useState } from 'react';

const OptionControl = (props) => {
	const [control, setControl] = useState("establishment")
	const handleControl = (e) => {
		props.handleControl(e.target.value)
		setControl(e.target.value)
	}
	return (
		<FormControl
			className="tw-w-full"
		>
			<RadioGroup
					row
					aria-labelledby="demo-row-radio-buttons-group-label"
					name="option"
			>
				<div className="tw-w-full tw-grid tw-grid-cols-2">
					<FormControlLabel value="" control={<Radio />} checked={control === ""} label="All" onChange={handleControl} />
					<FormControlLabel value="establishment" control={<Radio />} label={<div className="tw-truncate tw-w-[6rem] sm:tw-w-full">Establishment</div>} checked={control === "establishment"} onChange={handleControl} />
					<FormControlLabel value="address" control={<Radio />} checked={control === "address"} label="Address" onChange={handleControl} />
					<FormControlLabel value="geocode" control={<Radio />} checked={control === "geocode"} label="Geocode" onChange={handleControl} />
					<FormControlLabel value="(cities)" control={<Radio />} checked={control === "(cities)"} label="Cities" onChange={handleControl} />
					<FormControlLabel value="(regions)" control={<Radio />} checked={control === "(regions"} label="Region" onChange={handleControl} />
					<FormControlLabel value="strict-bounds" control={<Radio />} checked={control === "strict-bounds"} label="Strict Bounds" onChange={handleControl} />
					<div className="tw-col-span-2"><FormControlLabel value="bias" control={<Radio />} checked={control === "bias"} label="Bias to Map Viewport" onChange={handleControl} /></div>
				</div>
			</RadioGroup>
		</FormControl>
	)
}


export default OptionControl