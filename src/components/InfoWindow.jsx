const InfoWindowBox = () => {
	return (
		<div id="infowindow-content" className="tw-bg-white tw-w-full tw-h-max">
			<span id="place-name" className="title"></span>
			<span id="place-address"></span>
		</div>
	)
}

export default InfoWindowBox