import { Search } from "components";
import { Map } from "components";
import { useState } from "react";

const App = () => {
  const [types, setTypes] = useState([])
  const [position, setPosition] = useState(null)

  const handleControl = (v) => setTypes(v)
  const setMarker = (v) => setPosition(v)

  return (
    <div className="tw-relative">
      <div className="tw-fixed tw-z-10 tw-inset-0 tw-p-4 tw-bg-transparent tw-w-max tw-h-max">
        <Search handleControl={handleControl} setMarker={setMarker} />
      </div>
      <div className="tw-w-screen tw-h-screen">
        <Map types={types} currentPosition={position} />
      </div>
    </div>
  );
};

export default App;
