import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  history: [],
};

const mapSlice = createSlice({
  name: "MAP_REDUCER",
  initialState,
  reducers: {
    updateHistory: (state, action) => {
      state.history = [JSON.parse(action.payload), ...state.history]
    }
  },
});

export const {updateHistory} = mapSlice.actions;

export const historyList = (state) => state.mapReducer.history

export default mapSlice.reducer;