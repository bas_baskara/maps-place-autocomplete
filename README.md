# Getting Started

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## 1. Using npm:

   In the project directory, you can run:
   ### `npm install` and then `npm start`

## 2. Using docker:
   In the project directory, you can run:
   ### `docker compose up --build`
   or for older version docker-compose:
   ### `docker-compose up --build` 